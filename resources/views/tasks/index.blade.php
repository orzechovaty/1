@extends('layout')
@section('content')


	<div class="col-md-8 col-md-offset-2">
		<table class="table table-hover">
		<tr>
			<th>ID</th>
			<th>TITLE</th>
			<th>DESCRIPTION</th>
			<th>START</th>
			<th>END</th>
		</tr>
		@foreach($tasks as $task)
			<tr>
				<td>{{$task->idtasks}}</td>
				<td>{{$task->title}}</td>
				<td>{{$task->description}}</td>
				<td>{{$task->startdate}}</td>
				<td>{{$task->enddate}}</td>
				<td>
					{{ Form::open(['method' => 'DELETE', 'route' => ['tasks.destroy', $task->idtasks]]) }}
					    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
					{{ Form::close() }}
				</td>
			</tr>
		@endforeach
	</table>
	</div>
	
	
@endsection