@extends('layout')
@section('title', 'Add Task')
@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Add Task</h1>
		</div>
		<div class="col-md-8 col-md-offset-2">
		{!! Form::open(array('route' => 'tasks.store')) !!}
			{{Form::label('title', 'Title:')}}
			{{Form::text('title', null, array('class'=>'form-control'))}}
			{{Form::label('description', 'Desription:')}}
			{{Form::text('description', null, array('class'=>'form-control'))}}
			{{Form::label('description', 'Start date:')}}
			{{Form::date('startdate')}}<br/>
			{{Form::label('starthour', 'Start hour:')}}
			{{Form::text('starthour', null, array('class'=>'form-control'))}}
			{{Form::label('startminutes', 'Start minutes:')}}
			{{Form::text('startminutes', null, array('class'=>'form-control'))}}
			
			{{Form::date('enddate')}}<br/>
			{{Form::label('endhour', 'End hour:')}}
			{{Form::text('endhour', null, array('class'=>'form-control'))}}
			{{Form::label('endminutes', 'End minutes:')}}
			{{Form::text('endminutes', null, array('class'=>'form-control'))}}
			{{Form::submit('Add task!', array('class'=>'btn btn-success'))}}
		{!! Form::close() !!}
		</div>
		
	</div>

@endsection
