<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Tasks;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
	
	public function __construct(){
		$this->middleware('auth');
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	//$tasks = Tasks::paginate(2);
    	$tasks = Tasks::all();
    	$id = Auth::id();
		$tasks = Tasks::select(array('idtasks', 'title', 'description', 'startdate', 'enddate'))->where('user_id','=',$id)->get();
        //echo $id;
        return view('tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Tasks;
		$task->user_id = Auth::id();
		$task->title = $request->title;
		$task->description = $request->description;
		$task->startdate = $request->startdate." ".$request->starthour.":".$request->startminutes.":00";
		$task->enddate = $request->enddate." ".$request->endhour.":".$request->endminutes.":00";
		$task->save();
		
		return redirect()->route('tasks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	Tasks::where('idtasks', '=', $id)->delete();
        return redirect()->route('tasks.index');
    }
}
