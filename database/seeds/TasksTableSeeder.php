<?php

use Illuminate\Database\Seeder;
use App\Tasks;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task = new Tasks();
		$task->title = 'Tytuł1';
		$task->description = 'Opis1';
		$task->user_id = 1;
		$task->startdate = '2017-06-24 12:23:44';
		$task->enddate = '2017-06-24 12:55:00';
		$task->save();
		
		$task = new Tasks();
		$task->title = 'Tytuł2';
		$task->description = 'Opis2';
		$task->user_id = 2;
		$task->startdate = '2017-06-24 13:23:44';
		$task->enddate = '2017-06-24 13:55:00';
		$task->save();
		
		$task = new Tasks();
		$task->title = 'Tytuł3';
		$task->description = 'Opis3';
		$task->user_id = 3;
		$task->startdate = '2017-06-24 12:23:44';
		$task->enddate = '2017-06-24 12:55:00';
		$task->save();
    }
}
