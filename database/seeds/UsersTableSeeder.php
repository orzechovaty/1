<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
		$user->name = 'user1';
		$user->email = 'asd1@wp.pl';
		$user->password = bcrypt('user1');
		$user->save();
		
		$user = new User();
		$user->name = 'user2';
		$user->email = 'asd2@wp.pl';
		$user->password = bcrypt('user2');
		$user->save();
		
		$user = new User();
		$user->name = 'user3';
		$user->email = 'asd3@wp.pl';
		$user->password = bcrypt('user3');
		$user->save();
    }
}
